var exec  = require('child_process').exec,
	child;

var urlEncoder = function (array, callback) {

	var command = 'php getisourlencoded.php';
	for (var i=0; i<array.length; i++) {
		command += ' '+encodeURIComponent(array[i]);
	}

	child = exec(command,
		function (error, stdout, stderr) {
			callback(JSON.parse(stdout));
			if (error !== null) {
				console.log('exec error: ' + error);
			}
		});
};

module.exports = urlEncoder;

