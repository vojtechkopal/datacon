var ares = require('./aresLib.js');
var http = require("http"),
	url = require("url"),
	path = require("path"),
	fs = require("fs");

var port = process.argv[2] || 8080;
var encoding = 'utf-8';

http.createServer(function(request, response) {

	var success = false;
	var uri = url.parse(request.url).pathname
		, commands = uri.split("/");

	commands.splice(0,1);

	if (commands[0] == "ic") {
		var ic = commands[1];
		var aresConn = new ares();

		response.writeHead(200, {"Content-Type": "text/plain; charset="+encoding});
		aresConn.getPeopleByIC(ic, function (data) {
			response.write(JSON.stringify(data));
			response.end();
		});

		success = true;
	} else if (commands[0] == "person") {
		var chunks = decodeURIComponent(commands[1]).split(',');

		if (chunks.length >= 3) {
			var aresConn = new ares();

			var firstname = chunks[0];
			var lastname = chunks[1];
			var city = chunks[2];
			var street = chunks[3] || null;

			response.writeHead(200, {"Content-Type": "text/plain; charset="+encoding});
			aresConn.getSubjectsByPerson(firstname, lastname, city, street, function (data) {
				response.write(JSON.stringify(data));
				response.end();
			});

			success = true;
		} else {
			success = false;
		}
	} else if (commands[0] == "check") {
		var aresConn = new ares();
		var ics = commands[1].split(',');
		var table = {};
		var companies = {};

		var ifIcAvailableGetData = function () {
			if (ics.length > 0) {
				var ic = ics[0];
				ics.splice(0,1);
				aresConn.getPeopleByIC(ic, function (data) {
					var people = [];

					for (var i in data) {
						var person = data[i];
						var nameChunks = person.jmeno.split(' ');
						var string = '';
						var name;
						if (nameChunks.length > 2) {
							name = nameChunks[0] + ',' + nameChunks[nameChunks.length-1];
						} else {
							name = person.jmeno.replace(' ',',');
						}

						string += name+','+person.obec+','+person.adresa;

						people.push(string)
					}
					table[ic] = people;

					ifIcAvailableGetData();
				});
			} else {
				// get people
				var people = [];
				for (var key in table) {
					var subjekt = table[key];
					for (var index in subjekt) {
						var person = subjekt[index];
						if (people.indexOf(person) == -1) {
							people.push(person);
						}
					}
				}

				var ifPeopleFindSubjects = function () {
					if (people.length > 0) {
						var person = people[0];
						people.splice(0, 1);
						person = person; //aresConn.removeDiacritics(person);

						aresConn.getSubjectsByPersonString(person, function (data) {

							for (var index in data) {
								var item = data[index];
								var chunks = item.jmeno.split(' - ');
								var jmeno = '';
								if (chunks[0].indexOf('. ') != -1) {
									jmeno = chunks[0].split('. ')[1].replace(' ',',');
								} else {
									jmeno = chunks[0].replace(' ',',');
								}
								var adresa = chunks[1].split(', ');
								if (adresa.length > 2) adresa.splice(adresa.length - 2, 1);
								adresa[0] = adresa[0].replace(/ [0-9]+$/g,'');
								adresa = adresa.join(',');
								adresa = adresa.replace(/ [0-9\/]+$/g,'');

								var string = jmeno + ',' +adresa;
								//string = aresConn.removeDiacritics(string);

								if (table[item.ic]) {
									if (table[item.ic].indexOf(string) == -1) {
										table[item.ic].push(string);
									}
								} else {
									table[item.ic] = [string];
								}

								if (!companies[item.ic]) {
									companies[item.ic] = item.spolecnost;
								}
							}

							ifPeopleFindSubjects();
						});
					} else {
						var result = [];
						for (var ic in table) {
							var company = {
								'ic': ic,
								'people': table[ic]
							};

							if (companies[ic]) {
								company.name = companies[ic];
							}

							result.push(company);
						}

						response.writeHead(200, {"Content-Type": "text/plain; charset="+encoding});
						response.write(JSON.stringify(result));
						response.end();
					}
				}

				ifPeopleFindSubjects();
			}
		};

		ifIcAvailableGetData();
		success = true;
	}

	if (false == success) {
		response.writeHead(404, {"Content-Type": "text/plain; charset="+encoding, "Accept-Charset": encoding});
		response.write("404 Not Found ěščřžýáíé=\n");
		response.write(commands+"\n");
		response.end();
	}

}).listen(parseInt(port, 10));
