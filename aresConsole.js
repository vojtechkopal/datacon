var ares = require('./aresLib.js');
command = process.argv[2];
param = process.argv[3];

if (command == "ic") {
	var ic = param;
	var aresConn = new ares();

	aresConn.getPeopleByIC(ic, function (data) {
		console.log(JSON.stringify(data));
	});
} else if (command == "person") {
	var chunks = param.split(',');

	var firstname = chunks[0];
	var lastname = chunks[1];
	var city = chunks[2];
	var street = chunks[3] || null;

	var aresConn = new ares();
	aresConn.getSubjectsByPerson(firstname, lastname, city, street, function (data) {
		console.log(JSON.stringify(data));
	});
} else {
	console.log("unknown command");
}

