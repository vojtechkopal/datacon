<?php

$result = array();

for ($i = 1; $i < count($argv); $i++) {
  if (isset($argv[$i]) && $argv[$i] != "null") {
    $message = urldecode($argv[$i]);
    $result[] = urlencode(iconv("UTF-8", "iso-8859-2", $message));
  }
}

echo json_encode($result);

